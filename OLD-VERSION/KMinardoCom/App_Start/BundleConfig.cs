﻿using System.Web.Optimization;

namespace KMinardoCom
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            //jQuery CDN
            var jquery = new Bundle("~/bundles/jquery", "https://code.jquery.com/jquery-2.1.3.min.js")
                            .Include("~/Scripts/jquery-{version}.js");
            jquery.CdnFallbackExpression = "window.jQuery";
            bundles.Add(jquery);

            //Bootstrap CDN
            var bootstrap = new Bundle("~/bundles/bootstrap", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js")
                                .Include("~/Scripts/bootstrap.js");
            bootstrap.CdnFallbackExpression = "$.fn.modal";
            bundles.Add(bootstrap);

            //Angular CDN
            var angular = new Bundle("~/bundles/angular", "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js")
                .Include("~/Scripts/angular.js");
            angular.CdnFallbackExpression = "window.angular";
            bundles.Add(angular);

            //Locally hosted bundles
            bundles.Add(new Bundle("~/bundles/angularModules").Include(
                   "~/Scripts/angular-route.js",
                    "~/Scripts/angular-sanitize.js",
                    "~/Scripts/angular-sortable.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularapp")
                .IncludeDirectory("~/Web/", "*.js", true));
            
            bundles.Add(new Bundle("~/Content/lib").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/bootstrap-theme.css",
                    "~/Content/animate.css",
                    "~/Content/font-awesome.css",
                    "~/Content/devicon.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                    "~/Content/site.css",
                    "~/Content/site-animate.css",
                    "~/Content/site-print.css"));

#if (!DEBUG)
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
