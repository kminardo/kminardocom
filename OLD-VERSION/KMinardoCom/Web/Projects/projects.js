﻿angular.module('kminardoCom').controller('projectsController', ['$scope', 'projectsDataService',
  function ($scope, projectsDataService) {
      //Scope Variables

      //Private Variables

      //Scope Functions

      //Private Functions
      function activate() {
          $scope.projects = projectsDataService.get();
      };

      activate();
  }]);