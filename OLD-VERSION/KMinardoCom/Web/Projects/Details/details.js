﻿angular.module('kminardoCom').controller('projectDetailsController', ['$scope', '$routeParams', 'projectsDataService',
function ($scope, $routeParams, projectsDataService) {
    //Scope Variables

    //Private Variables

    //Scope Functions

    //Private Functions
    function activate() {
        $scope.project = projectsDataService.get($routeParams.projectId);
        $scope.test = $scope.project.imgs;
    };

    activate();
}]);