﻿angular.module('kminardoCom').service('projectsDataService', [
  function () {
      var fakeDescription = 'Bacon ipsum dolor amet ham hock fatback picanha pork loin pastrami, chuck alcatra prosciutto boudin short loin filet mignon. Salami tail pork bacon ball tip. Pig tenderloin alcatra shank turkey ham andouille. Turkey corned beef boudin chuck porchetta pork belly pastrami. Sirloin chuck doner pancetta. Andouille short ribs leberkas tenderloin frankfurter tongue chicken turducken picanha tri-tip brisket sirloin. Kevin filet mignon boudin, chuck short ribs ham beef. Swine alcatra corned beef frankfurter brisket landjaeger strip steak ham filet mignon. Drumstick frankfurter beef, ground round spare ribs ribeye pork. Cupim rump pig, pancetta ribeye flank ground round tail short ribs shoulder kevin beef ribs jerky jowl swine. Pork loin pork chop alcatra pig pork frankfurter jowl filet mignon biltong turducken boudin swine kielbasa corned beef venison. Hamburger cupim boudin bacon flank leberkas ribeye frankfurter rump. Porchetta beef ground round short ribs landjaeger meatball, strip steak frankfurter. Pork shank turkey andouille picanha pork loin corned beef. Bacon beef ribs ground round hamburger biltong tri-tip pork. Corned beef pastrami prosciutto, rump ham meatloaf porchetta tongue turducken bresaola jerky sirloin filet mignon. Meatloaf short ribs jerky tenderloin biltong, short loin kevin.';

      var projectStatus = {
          OnHold: '<i class="fa fa-exclamation-circle"></i> On Hold',
          Released: '<i class="fa fa-check-circle"></i> Released',
          InDev: '<i class="fa fa-cog"></i> In Development',
          Agile: '<i class="fa fa-refresh"></i> Agile - In Development'
      };

      var projectTechnologies = {
          AngularJS: {
              name: 'AngularJS',
              link: 'https://angularjs.org/',
              icon: 'devicon-angularjs-plain'
          },
          JQuery: {
              name: 'JQuery',
              link: 'http://jquery.com/',
              icon: 'devicon-jquery-plain-wordmark'
          },
          DotNet: {
              name: '.NET',
              link: 'http://www.microsoft.com/net/',
              icon: 'devicon-dot-net-plain-wordmark'
          },
          Git: {
              name: 'Git',
              link: 'http://git-scm.com/',
              icon: 'devicon-git-plain-wordmark'
          },
          Javascript: {
              name: 'Javascript',
              link: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/',
              icon: 'devicon-javascript-plain'
          },
          Oracle12c: {
              name: 'Oracle 12c',
              link: '',
              icon: ''
          },
          WinForm: {
              name: 'WinForm',
              link: '',
              icon: ''
          },
          TFS: {
              name: 'TFS',
              link: '',
              icon: ''
          },
          KendoUI: {
              name: 'KendoUI',
              link: '',
              icon: ''
          }
      }

      var projectData = [
        {
            name: 'SPI Glass',
            imgs: ['http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536'],
            startDate: new Date('7-22-2013'),
            status: projectStatus.Agile,
            description: fakeDescription,
            technologies: [projectTechnologies.Javascript, projectTechnologies.AngularJS, projectTechnologies.DotNet, projectTechnologies.Oracle12c, projectTechnologies.Git],
            client: 'Safeguard Properties',
            codeUrl: '',
            demoUrl: ''
        },
        {
            name: 'AccountTrackr',
            imgs: ['/Content/Images/Projects/AccountTrackr/NewItem.png',
                    '/Content/Images/Projects/AccountTrackr/Inventory.png',
                    '/Content/Images/Projects/AccountTrackr/NewCategory.png',
                    '/Content/Images/Projects/AccountTrackr/Customers.png'],
            startDate: new Date('01-12-2014'),
            status: projectStatus.OnHold,
            description: fakeDescription,
            technologies: [projectTechnologies.Javascript, projectTechnologies.AngularJS, projectTechnologies.JQuery, projectTechnologies.DotNet, projectTechnologies.Git],
            client: 'Personal',
            codeUrl: 'https://bitbucket.org/kminardo/accounttrackr',
            demoUrl: 'http://accounttrackr.kminardo.com'
        },
        {
            name: 'FileImporter',
            imgs: ['http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536'],
            startDate: new Date('9-1-2012'),
            status: projectStatus.Released,
            description: fakeDescription,
            technologies: [projectTechnologies.DotNet, projectTechnologies.WinForm, projectTechnologies.TFS],
            client: 'Safeguard Properties',
            codeUrl: '',
            demoUrl: ''
        },
        {
            name: 'WDS - Role Master',
            imgs: ['http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536'],
            startDate: new Date('7-1-2012'),
            status: projectStatus.Released,
            description: fakeDescription,
            technologies: [projectTechnologies.Javascript, projectTechnologies.KendoUI, projectTechnologies.JQuery, projectTechnologies.DotNet, projectTechnologies.Oracle12c, projectTechnologies.TFS],
            client: 'Safeguard Properties',
            codeUrl: '',
            demoUrl: ''
        },
        {
            name: 'Billing Assistant',
            imgs: ['http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536'],
            startDate: new Date('5-1-2011'),
            status: projectStatus.Released,
            description: fakeDescription,
            technologies: [projectTechnologies.WinForm, projectTechnologies.DotNet, projectTechnologies.TFS],
            client: 'Safeguard Properties',
            codeUrl: '',
            demoUrl: ''
        },
        {
            name: 'KMinardoCom',
            imgs: ['http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536', 'http://placehold.it/2048x1536'],
            startDate: new Date('12-10-2014'),
            status: projectStatus.Agile,
            description: fakeDescription,
            technologies: [projectTechnologies.Javascript, projectTechnologies.AngularJS, projectTechnologies.JQuery, projectTechnologies.DotNet, projectTechnologies.Git],
            client: 'Personal',
            codeUrl: 'https://bitbucket.org/kminardo/kminardocom',
            demoUrl: 'http://kminardo.com'
        }
      ];

      this.get = function (id) {
          if (id) {
              var project = projectData.filter(function (el) {
                  return el.name == id;
              });

              if (project.length > 0)
                  return project[0];
          } else {
              return projectData;
          }
      }
  }]);
