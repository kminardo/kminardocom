﻿angular.module('kminardoCom').service('markdownLite', [
  function () {
      var listCount;

      this.textToHTML = function (text) {
          textLines = text.split(/\r\n|\r|\n/g);

          //Append a null line at the end to finish processing
          textLines.push(null);

          //Reset the listCount
          listCount = 0;

          for (var i = 0; i < textLines.length; i++) {
              var textLine = textLines[i];
              textLines[i] = parseListToHTML(textLine);
          }

          return textLines.join('');
      };

      this.HTMLtoText = function (htmlString) {
          var elements = $.parseHTML(htmlString);
          var returnString = '';

          for (var i = 0; i < elements.length; i++) {
              switch (elements[i].nodeType) {
                  case Node.ELEMENT_NODE:
                      switch (elements[i].nodeName) {
                          case 'UL':
                              for (var e = 0; e < elements[i].childNodes.length; e++) {
                                  var childNode = elements[i].childNodes[e];
                                  if (childNode.textContent) {
                                      returnString += '*' + childNode.textContent + '\r';
                                  }
                              }
                              break;
                      }
                      break;
                  case Node.TEXT_NODE:
                      returnString += elements[i].textContent + '\r';
                      break;
                  default:
                      returnString += elements[i].outerHTML + '\r';
              }

              //Slice off the last return if it's the last item
              if (i == elements.length - 1) {
                  returnString = returnString.slice(0, -1);
              }
          }

          return returnString;
      }

      function parseListToHTML(textLine) {
          if (textLine && textLine.indexOf('*') > -1) {

              //start of a new list
              if (listCount == 0) {
                  textLine = '<ul>' + textLine;
              }

              textLine = textLine.replace('\*', '<li>');
              textLine += '</li>';

              listCount++;

          } else if (listCount > 0) {
              textLine = textLine ? '</ul>' + textLine : '</ul>';
              listCount = 0;
          }

          return textLine;
      };
  }]);
