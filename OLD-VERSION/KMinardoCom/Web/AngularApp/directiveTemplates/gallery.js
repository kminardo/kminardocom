﻿angular.module('kminardoCom').controller('galleryController', ['$scope',
  function ($scope) {
      //$scope.photos set in element attribute
      
      $scope.setCurrent = function (photo) {
          $scope.currentPhoto = photo;
      };

      $scope.showGalleryModal = function () {
          $('#galleryModal').modal('show');
      };

      function activate() {
          $scope.setCurrent($scope.photos[0]);
      };

      activate();
  }]);