﻿angular.module('kminardoCom').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        title: 'Resume',
        templateUrl: '/Web/Resume/resume.html',
        controller: 'resumeController',
        caseInsensitiveMatch: true
        //    title: 'Home',
        //    templateUrl: '/Web/Index/index.html',
        //    controller: 'TestPage'
    })
    .when('/Resume', {
        title: 'Resume',
        templateUrl: '/Web/Resume/resume.html',
        controller: 'resumeController',
        caseInsensitiveMatch: true
    })
    .when('/Projects', {
        title: 'Projects',
        templateUrl: '/Web/Projects/projects.html',
        controller: 'projectsController',
        caseInsensitiveMatch: true
    })
    .when('/Projects/:projectId', {
        title: function (params) { return params.projectId },
        templateUrl: '/Web/Projects/Details/details.html',
        controller: 'projectDetailsController',
        caseInsensitiveMatch: true
    })
    .when('/Contact', {
        title: 'Contacts',
        templateUrl: '/Web/Misc/comingsoon.html',
        caseInsensitiveMatch: true
    })
    .otherwise({
        redirectTo: '/Resume'
    });

    $locationProvider.html5Mode({ enabled: true, requireBase: false });
}]);