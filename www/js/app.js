angular.module('kminardoCom', ['ngRoute', 'ngSanitize']);

angular.module('kminardoCom').controller('LayoutController', ['$scope', '$window', function ($scope, $window) {
    $scope.closeMenu = function () {
        //Eww dirty Jquery in a controller. Sorry.
        if ($window.innerWidth <= 768) {
            $(".navbar-collapse").collapse('hide');
        };
    };
}]);