﻿angular.module('kminardoCom').run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if (typeof (current.$$route.title) == "function")
        {
            $rootScope.dynamicTitle = current.$$route.title(current.params);
        } else {
            $rootScope.dynamicTitle = current.$$route.title;
        }

        $rootScope.dynamicTitle += ' - KMinardo.Com';
    });
}]);