﻿//Partial controller, view expects the following
//$scope.skills = [{
//    name: <name>,
//    rating: <int 1-5>
//}];


angular.module('kminardoCom').controller('StarRatingController', ['$scope', '$window',
    function ($scope, $window) {
        $scope.$watchCollection('skills', function (newSkills, oldSkills) {
            calculateSkillRows();
        });

        $scope.$on('calculateSkills', function (event, args) {
            calculateSkillRows(args.columnOverride);
        });

        function calculateSkillRows(columnOverride) {
            $scope.skillRows = [];
            var skillRow = [];
            var columns = columnOverride ? columnOverride : $window.innerWidth <= 768 ? 3 : 4;

            for (var i = 1; i < $scope.skills.length + 1; i++) {
                skillRow.push($scope.skills[i - 1]);
                if (i % columns == 0) {
                    $scope.skillRows.push(skillRow);
                    skillRow = [];
                }
            }

            //push the last column set
            $scope.skillRows.push(skillRow);
        }

        function activate() {
            //calculateSkillRows();
        }

        activate();
    }]);