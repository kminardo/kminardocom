﻿angular.module('kminardoCom').controller('resumeController', ['$scope', '$location', '$timeout', '$anchorScroll', 'markdownLite',
  function ($scope, $location, $timeout, $anchorScroll, markdownLite) {
      //Scope Variables
      $scope.resumeSections = [];
      $scope.skills = [];
      $scope.contactInfo = [];

      //Private Variables
      var tempEditStorage = {};

      //Scope Functions
      $scope.addNewResumeSection = function (location) {
          var newSection = new section('');
          newSection.editable = true;

          $scope.resumeSections.push(newSection);
      };

      $scope.addNewSkill = function () {
          var newSkill = new skill();
          newSkill.editable = true;
          $scope.skills.push(newSkill);
      };

      $scope.makeEditable = function (obj) {
          tempEditStorage[obj.id] = angular.copy(obj);
          obj.editable = true;
      };

      $scope.cancelEditable = function (obj, scopeName) {
          if (tempEditStorage[obj.id]) {
              if (IsArray($scope[scopeName])) {
                  var arrayIndex = $scope[scopeName].indexOf(obj);
                  $scope[scopeName][arrayIndex] = angular.copy(tempEditStorage[obj.id]);
                  revertArrayByIndex($scope[scopeName]);
              } else {
                  $scope[scopeName] = angular.copy(tempEditStorage[obj.id]);
              }
          } else {
              $scope.removeSection(obj, scopeName);
          }
      };

      $scope.moveSection = function (obj, arrayName, direction) {
          var moveToIndex;
          var currentIndex = $scope[arrayName].indexOf(obj);

          if (direction == 'up') {
              moveToIndex = currentIndex - 1;

          } else if (direction == 'down') {
              moveToIndex = currentIndex + 1;
          }

          $scope[arrayName].splice(moveToIndex, 0, $scope[arrayName].splice(currentIndex, 1)[0]);
      };

      $scope.formatContentToHTML = function (section, arrayName) {
          if (section.content) {
              section.content = markdownLite.textToHTML(section.content);
          } else {
              $scope.removeSection(section, arrayName);
          }
      };

      $scope.toggleSetting = function (setting) {
          $scope.userSettings[setting] = !$scope.userSettings[setting];
          saveUserSettings();
      };

      $scope.formatHTMLFromContent = function (section) {
          section.content = markdownLite.HTMLtoText(section.content);
      };

      $scope.starClick = function (skill, num) {
          skill.rating = num;
      };

      $scope.removeSection = function (obj, arrayName) {
          $scope[arrayName] = $scope[arrayName].filter(function (el) {
              return el.id !== obj.id;
          });

          $scope.saveResume();
      };

      $scope.saveResume = function () {
          //KMTODO: should be saved to a database
          itemsToSave = [$scope.resumeSections, $scope.skills, $scope.contactInfo];

          for (var i = 0; i < itemsToSave.length; i++) {
              currentItem = itemsToSave[i];

              if (IsArray(currentItem)) {
                  updateArrayIndexs(currentItem);

                  for (var a = 0; a < currentItem.length; a++) {
                      var section = currentItem[a];

                      //Flip sections to non-editable
                      //Empty content should NOT be flipped
                      //Prevents empty elements from being added to page
                      if ((section.rating || section.content) && (section.editable || section.editable === false)) {
                          section.editable = null;
                      }
                  }
              } else {
                  currentItem.editable = null;
              }
          }

          localStorage.setItem('fullResume', angular.toJson($scope.resumeSections));
          localStorage.setItem('skills', angular.toJson($scope.skills));
          localStorage.setItem('contact', angular.toJson($scope.contactInfo));
          localStorage.setItem('settings', angular.toJson($scope.userSettings));
      };

      $scope.loadResume = function () {
          //KMTODO: should be loaded out of a database
          var resumeSections = angular.fromJson(localStorage.getItem('fullResume'));
          var skills = angular.fromJson(localStorage.getItem('skills'));
          var contactInfo = angular.fromJson(localStorage.getItem('contact'));
          var userSettings = angular.fromJson(localStorage.getItem('settings'));

          if (resumeSections && skills && contactInfo && userSettings) {
              $scope.resumeSections = resumeSections;
              $scope.skills = skills;
              $scope.contactInfo = contactInfo;
              $scope.userSettings = userSettings;
          } else {
              $scope.resetResume(true);
          }
      };

      $scope.resetResume = function (skipConfirm) {
          var defaultResumeSections = [{ "id": 1419117308429, "content": "Career Objective", "isTitle": true, "index": 0 }, { "id": 1419117314248, "content": "Obtain a challenging and rewarding position with a reputed organization which will help me deliver my best and upgrade my skills in software development while meeting the demands of the agency.", "isTitle": false, "editable": null, "index": 1 }, { "id": 1419117320408, "content": "Qualification Highlights", "isTitle": true, "index": 2 }, { "id": 1419117453415, "content": "<ul><li>Promoted consistently year over year to positions of increasing complexity and authority throughout career.</li><li>Skilled in prioritizing and managing multiple tasks simultaneously while mastering tools, processes, and innovative software.</li><li>Demonstrated competence in building, testing, troubleshooting and documenting software.</li><li>Exhibited high quality, results-driven professional customer service and support to instill confidence in technical advice and directions.</li><li>Detail oriented and dedicated to excellence in building fast, easily modified software to meet any customer demands.</li></ul>", "isTitle": false, "editable": null, "index": 3 }, { "id": 1419117505054, "content": "Experience", "isTitle": true, "editable": null, "index": 4 }, { "id": 1423451289392, "content": "Safeguard Properties, Brooklyn Heights, OH", "isTitle": false, "isSubtitle": true, "editable": null, "index": 5 }, { "id": 1423451304324, "content": "Application Developer, 2012 - Present", "isSubtitle": true, "editable": null, "index": 6 }, { "id": 1423451319809, "content": "<ul><li>Architected the user interface for Safeguard's Next Generation order review software called SPI Glass</li><li>Work based on Photoshop designs (PSD) from Design Interactive and Lvl7</li><li>An initial proponent of a new source control system, performed the moving of code from TFS to Git</li><li>Participated in a newly established Agile/SCRUM environment</li><li>Collaborated with project management and help desk to troubleshoot and resolve application errors in a timely fashion</li></ul>", "editable": null, "index": 7 }, { "id": 1423451573296, "content": "-- Application Highlights", "isSubtitle": true, "editable": null, "index": 8 }, { "id": 1423451331868, "content": "<ul><li>SPI Glass - A web based GUI application designed to replace older command line BBj software. Considered the heart of the company, this large project is still under development.</li><li>I contributed to SPI Glass by designing and building the front end using AngularJS, C# and ASP.NET WebAPI. I was also tasked with several \"back end\" workers to handle flow and sharing of data across the many systems Safeguard is responsible for.</li></ul>", "editable": null, "index": 9 }, { "id": 1423451344449, "content": "Safeguard Properties, Brooklyn Heights, OH", "isSubtitle": true, "editable": null, "index": 10 }, { "id": 1423451349028, "content": "IT Support Specialist, 2011 - 2012", "isSubtitle": true, "editable": null, "index": 11 }, { "id": 1423451360982, "content": "<ul><li>Worked as a team of one to develop small programs to support employees and improve productivity</li><li>Collaborated with business leaders to gather requirements and expectations</li><li>Built wireframes to express program flow and UI interaction</li></ul>", "editable": null, "index": 12 }, { "id": 1423451573295, "content": "-- Application Highlights", "isSubtitle": true, "editable": null, "index": 13 }, { "id": 1423451363358, "content": "<ul><li>Billing Assistant - An improved system to automatically script the invoicing of clients - Initially built in Excel using VBA, later converted to C#</li><li>FileImporter - Improving the process to upload images to orders, eliminating a support department role - Written using C#</li><li>Role Master - A general permissions application to easily grant access to critical software and components - Written using HTML, Javascript (Jquery, KendoUI controls) and ASP.NET</li></ul>", "editable": null, "index": 14 }, { "id": 1419117865842, "content": "Safeguard Properties, Mentor, OH", "isSubtitle": true, "editable": null, "index": 15 }, { "id": 1419117964449, "content": "REO Specialist, 2011 - 2012", "isSubtitle": true, "index": 16 }, { "id": 1419117980138, "content": "<ul><li>Inspected and examined contractors work and filed for accuracy, legibility, and damages.</li><li>Reviewed orders for completeness according to client parameters and followed up on incomplete orders.</li><li>Inspected outgoing work for compliance with customer's specifications.</li><li>Generated invoices for clients and managed payment to company contractors.</li></ul>", "isSubtitle": false, "editable": null, "index": 17 }, { "id": 1419118153030, "content": "Officemax, Willoughby Hills, OH", "isSubtitle": true, "index": 18 }, { "id": 1419118157294, "content": "Technology Supervisor, 2008 - 2011", "isSubtitle": true, "index": 19 }, { "id": 1419118202984, "content": "<ul><li>Supervised a staff of 12 sales associates, including recommendations for hiring, sales training and employee development</li><li>Advised customers on new technology, trends, and product features</li><li>Skillfully resolved all customer service issues to mutual satisfaction</li><li>Counted cash; and reconciled charge sales and cash receipts with total sales to verify accuracy of transactions. Received change orders and balanced store safe nightly.</li></ul>", "isSubtitle": false, "editable": null, "index": 20 }, { "id": 1419118212781, "content": "Self-Employed, Eastlake, OH", "isSubtitle": true, "index": 21 }, { "id": 1419118216395, "content": "Computer Technician, 2008 - Present", "isSubtitle": true, "index": 22 }, { "id": 1419118257355, "content": "<ul><li>Performed in-home repair of computer systems and related equipment by removing and replacing components or reconfiguring and restoring systems</li><li>Diagnosed hardware failure, user errors</li><li>Knowledgeable in computer software, hardware and procedures</li><li>References from clients available upon request</li></ul>", "isSubtitle": false, "editable": null, "index": 23 }, { "id": 1419118263704, "content": "K-Mart, Eastlake, OH", "isSubtitle": true, "index": 24 }, { "id": 1419118269580, "content": "Department Leader, 2006 - 2008", "isSubtitle": true, "index": 25 }, { "id": 1419118305644, "content": "<ul><li>Developed personal relationships with customers to ensure loyalty and retention</li><li>Acquired an expertise in customer service and \"closing the sale\"</li><li>Participated in receiving, check-in, and stocking of merchandise, prepared products for sale, demonstrated new products, and trained co-workers in use and application of new products</li><li>Operated cash register and other general duties as assigned</li></ul>", "isSubtitle": false, "editable": null, "index": 26 }, { "id": 1419118311954, "content": "Education", "isTitle": true, "isSubtitle": false, "index": 27 }, { "id": 1419118319869, "content": "B.S., Business Administration, 2012", "isTitle": false, "isSubtitle": true, "index": 28 }, { "id": 1419118323605, "content": "Lakeland Community College, Kirtland, OH", "isTitle": false, "isSubtitle": true, "index": 29 }, { "id": 1419118381807, "content": "<ul><li>Incomplete - Transfer program with Cleveland State University to obtain a Bachelor's Degree</li></ul>", "isTitle": false, "isSubtitle": false, "editable": null, "index": 30 }, { "id": 1419118389489, "content": "H.S. Diploma, 2008", "isTitle": false, "isSubtitle": true, "editable": null, "index": 31 }, { "id": 1419118397223, "content": "North High School, Eastlake, OH", "isTitle": false, "isSubtitle": true, "editable": null, "index": 32 }, { "id": 1419118402272, "content": "<ul><li>Completed</li></ul>", "isTitle": false, "isSubtitle": false, "editable": null, "index": 33 }, { "id": 1419118410501, "content": "Soft Skills", "isTitle": true, "isSubtitle": false, "index": 34 }, { "id": 1419118457997, "content": "<ul><li>Customer Service</li><li>Time Management</li><li>Problem Solving/Troubleshooting</li><li>Supervision and Training</li><li>End-User Computer Training</li></ul>", "isTitle": false, "isSubtitle": false, "editable": null, "index": 35 }]
          var defaultSkills = [{ "name": "Javascript", "rating": 5, "id": 1421868237501, "index": 0 }, { "name": "HTML", "rating": 5, "id": 1421868237502, "index": 1 }, { "name": "CSS/SASS", "rating": 5, "id": 1421868237503, "index": 2 }, { "name": "Bootstrap", "rating": 5, "id": 1421868237504, "index": 3 }, { "name": "Angular", "rating": 5, "id": 1421868237505, "index": 4 }, { "name": "C#", "rating": 3, "id": 1421868237506, "index": 5 }, { "name": "VB.NET", "rating": 4, "id": 1421868237507, "index": 6 }, { "name": "MS/Oracle SQL", "rating": 3, "id": 1421868237508, "index": 7 }, { "name": "Web API/MVC4", "rating": 4, "id": 1421868237509, "index": 8 }, { "name": "Yeoman, Grunt, etc.", "rating": 4, "id": 1421868237510, "index": 9 }, { "name": "Node", "rating": 2, "id": 1421868237511, "editable": null, "index": 10 }, { "name": "Git/TFS", "rating": 4, "id": 1421868237512, "index": 11 }, { "name": "Python/Django", "rating": 1, "id": 1421868237513, "index": 12 }, { "name": "Ruby on Rails", "rating": 1, "id": 1421868237514, "index": 13 }];
          var defaultResumeContactInfo = { "id": 1425486070272, "name": "Kenneth A. Minardo Jr.", "address1": "25200 Rockside Rd", "address2": "", "city": "Bedford Heights", "state": "OH", "zipcode": "44146", "phone1": "440.728.6202", "email": "KMinardo@gmail.com" };

          if (skipConfirm || confirm('Are you sure?')) {
              $scope.resumeSections = defaultResumeSections;
              $scope.skills = defaultSkills;
              $scope.contactInfo = defaultResumeContactInfo;
              $scope.userSettings = {
                  showWelcome : true
              }

              $scope.saveResume();
          }
      };

      $scope.print = function () {
          $scope.$broadcast('calculateSkills', { columnOverride: 3 });
          window.print();
          $timeout(function () { $scope.$broadcast('calculateSkills', { columnOverride: null }); }, 100);
      };

      //Private Functions
      function activate() {
          $scope.loadResume();
      };

      function saveUserSettings() {
          localStorage.setItem('settings', angular.toJson($scope.userSettings));
      };

      function section(content, isTitle, isSubtitle) {
          return {
              id: Date.now(),
              content: content,
              isTitle: isTitle,
              isSubtitle: isSubtitle,
          }
      };

      function skill(name, rating) {
          return {
              id: Date.now(),
              name: name,
              rating: rating ? rating : 1,
          }
      };

      function updateArrayIndexs(arrayObj) {
          for (var i = 0; i < arrayObj.length; i++) {
              arrayObj[i].index = i;
          }
      };

      function revertArrayByIndex(arrayObj) {
          for (var i = 0; i < arrayObj.length; i++) {
              arrayObj.splice(arrayObj[i].index, 0, arrayObj.splice(i, 1)[0]);
          }
      };

      function IsArray(obj) {
          return Object.prototype.toString.call(obj) == "[object Array]";
      };



      activate();
  }]);