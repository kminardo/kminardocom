#WARNING
New version coming soon, this README is out of date!

#About

This code is hosted on kminardo.com - It's mainly a personal page but it does have a few goodies mixed in. Note that some of the projects displayed on the page might be hosted under different repositories (AccountTrackr, for example).

###Language
- Database: None
- Web Server: C#, WebAPI, MVC5
- Client: HTML, Javascript, AngularJS

###Requirements
- Database: None
- Web Server: IIS 7 or later
- Server Framework: .NET 4.5
- Client Framework/Libraries: AngularJS v1.3.4, Bootstrap v3.3.1, jQuery v2.1.1
****All libraries should be pulled automatically from NuGet
- Developer fueled by Great Lakes Beer